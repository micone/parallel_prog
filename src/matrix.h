#ifndef _matrix_h
#define _matrix_h

#include <random>
#include <stdlib.h>

struct Matrix {
    double **_data;
    /* double *data; */
    /* vector<vector<double>> _data */
    size_t size;
public:
    Matrix(size_t n) {
        size = n;
        _data = static_cast<double **>(malloc(n * sizeof(double *)));
        for (size_t i = 0; i < n; ++i) {
            _data[i] = static_cast<double *>(calloc(n, sizeof(double)));
        }
    }
    void rand();
    /*
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_ditribution<double> dist(1.0, 10.0);
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size; ++j) {
            _data[i][j] = dist(gen);
        }
    }
    or 
    for (auto & row: data) {
        for (auto & elem: row) {
            elem = dist(gen);
        }
    }
    ---------------------------------------------------------------- 
    auto start = chrono::steady_clock::now();
    auto end = chono::steady_clock::now();
    auto diff = end - start;
    int time = chrono::duration_cast<chrono::milliseconds>(diff).count();
    */
};

#endif /* _matrix_h */
