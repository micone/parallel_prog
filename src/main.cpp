#include <iostream>
#include <chrono>
#include "matrix.h"

int main(void)
{
    int n;
    std::cout << "Hello, world!\n" << std::endl;
    std::cout << "> ";
    std::cin >> n;
    Matrix a = Matrix(n);
    auto start = std::chrono::steady_clock::now();
    a._data[0][0] = 1;
    a.rand();
    auto end = std::chrono::steady_clock::now();
    auto diff = end - start;
    int time = std::chrono::duration_cast<std::chrono::milliseconds>(diff).count();
    std::cout << "Randomization time: " << time << "ms" << std::endl;
    return 0;
}
